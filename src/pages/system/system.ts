import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-system',
  templateUrl: 'system.html'
})
export class SystemPage {
  serverUrl:any
  
  constructor(
    private storage: Storage,
    public navCtrl: NavController,
    private sanitizer: DomSanitizer) {
    this.storage.get('appUrl').then((val) => {
      this.serverUrl = this.sanitizer.bypassSecurityTrustResourceUrl(val)
    })
  }
}
