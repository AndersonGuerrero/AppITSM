import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { SystemPage } from '../../pages/system/system';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    private storage: Storage, 
    public navCtrl: NavController,
    public loadingCtrl: LoadingController, 
    public alertCtrl: AlertController) {
    this.storage.get('appUrl').then((val) => {
      if(val){
        this.navCtrl.push(SystemPage);
      }
    });
  }

  ngOnInit() {
    this.showLoading()
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Error!',
      subTitle: 'Por favor ingrese los datos requeridos!',
      buttons: ['OK']
    });
    alert.present();
  }

  showLoading() {
    const loader = this.loadingCtrl.create({
      content: "Espere un momento...",
      duration: 3000
    });
    loader.present();
  }

  saveServer(url: string){
    if(url){
      this.storage.set('appUrl', url);
      this.navCtrl.push(SystemPage);
    }else{
      this.showAlert()
    }
  }
}
